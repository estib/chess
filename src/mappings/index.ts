import cP from "./chessPieces.json";
import cPG from "./chessPiecesGif.json";
import iP from "./initialPosition.json";
import pC from "./pieceColors.json";
import pT from "./pieceType.json";
import pV from "./pieceValue.json";
import g from "./gifs.json";
import pM from "./possibleMovesMap.json";
import pMD from "./possibleMoveDirectionsMap.json";
import { MoveDirections } from "../logic";

export interface PositionMap {
    [key: string]: ChessPieceKey;
}

export interface PossibleMovesMap {
    [tileKey: string]: string[];
}

export interface PossibleMoveDirectionsMap {
    [tileKey: string]: MoveDirections;
}

/**
 * Map of grouped moves for a given piece and tile position.
 *
 * Meant to be used in the possible move determination
 */
export interface PiecePossibleMoveDirectionsMap {
    ["king-black"]: PossibleMoveDirectionsMap;
    ["knight-black"]: PossibleMovesMap;
    ["king-white"]: PossibleMoveDirectionsMap;
    ["knight-white"]: PossibleMovesMap;
    ["pawn-white"]: PossibleMoveDirectionsMap;
    ["pawn-black"]: PossibleMoveDirectionsMap;
    ["queen-black"]: PossibleMoveDirectionsMap;
    ["queen-white"]: PossibleMoveDirectionsMap;
    ["bishop-black"]: PossibleMoveDirectionsMap;
    ["bishop-white"]: PossibleMoveDirectionsMap;
    ["rock-black-left"]: PossibleMoveDirectionsMap;
    ["rock-black-right"]: PossibleMoveDirectionsMap;
    ["rock-white-left"]: PossibleMoveDirectionsMap;
    ["rock-white-right"]: PossibleMoveDirectionsMap;
}

/**
 * Map of flat list of moves for a given piece and tile position.
 *
 * Meant to be used in the coverage determination
 */
export interface PiecePossibleMovesMap {
    [piece: string]: PossibleMovesMap;
}

export const chessPiecesDefaultMap = cP;
export const chessPiecesGifMap = cPG;
export const initialPositionMap: PositionMap = iP as PositionMap;
export const pieceColors = pC;
export const pieceTypes = pT;
export const pieceValue = pV;
export const gifs = g;
export const possibleMovesMap = pM as PiecePossibleMovesMap;
export const possibleMoveDirectionsMap = pMD as PiecePossibleMoveDirectionsMap;

export type ChessPieceKey = keyof typeof chessPiecesDefaultMap;
export type PieceMap = typeof chessPiecesDefaultMap;
