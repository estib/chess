import React from "react";
import { Piece } from "./Piece";
import { PieceMap } from "../mappings";
import { TileInformation, getTileColorClass, TileColor } from "../logic";

export interface TileProps extends TileInformation {
    onClick: () => void;
    color: TileColor;
    theme: PieceMap;
    highlighted: boolean;
}

export const Tile: React.FC<TileProps> = (props) => {
    const tileClass = getTileColorClass(props.color, props.highlighted);
    return (
        <div className={tileClass} onClick={props.onClick}>
            {props.piece !== undefined ? <Piece piece={props.piece} theme={props.theme} /> : <React.Fragment />}
        </div>
    );
};
