import React from "react";
import {
    TileInformation,
    TileRank,
    TileFile,
    RANK_DIMENSION,
    FILE_DIMENSION,
    fileMap,
    rankMap,
    getTileColor,
    createTileKey,
} from "../logic";
import { Tile, TileProps } from "./Tile";
import { ChessPieceKey, PieceMap } from "../mappings";

export interface GameBoardProps {
    clickHandler: (tileProps: TileInformation) => void;
    getPieceForTile: (rank: TileRank, file: TileFile) => ChessPieceKey | undefined;
    invert: boolean;
    theme: PieceMap;
    block: boolean;
    highlightedTiles: string[];
    hide: boolean;
}

export const GameBoard: React.FC<GameBoardProps> = (props) => {
    const tiles: TileProps[] = [];

    for (let _i = 0; _i < RANK_DIMENSION; _i++) {
        for (let _j = 0; _j < FILE_DIMENSION; _j++) {
            const i = props.invert ? _i : RANK_DIMENSION - 1 - _i;
            const j = props.invert ? FILE_DIMENSION - 1 - _j : _j;
            const file = fileMap[j];
            const rank = rankMap[i];

            const tileKey = createTileKey(rank, file);
            const highlighted = props.highlightedTiles.includes(tileKey);

            const tileInfo: TileInformation = {
                file,
                rank,
                piece: props.getPieceForTile(rank, file),
            };

            const onClick = () => props.clickHandler(tileInfo);

            const tileProps: TileProps = {
                ...tileInfo,
                onClick,
                theme: props.theme,
                color: getTileColor(i, j),
                highlighted,
            };

            tiles.push(tileProps);
        }
    }

    const cssClass = props.hide ? "game-board-hide" : props.block ? "game-board-block" : "game-board";

    return (
        <div className={cssClass}>
            {tiles.map((t, i) => (
                <Tile {...t} key={i} />
            ))}
        </div>
    );
};
