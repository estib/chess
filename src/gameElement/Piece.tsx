import React from "react";
import { ChessPieceKey, PieceMap } from "../mappings";

interface PieceProps {
    piece: ChessPieceKey;
    theme: PieceMap;
}

export const Piece: React.FC<PieceProps> = (props) => {
    const theme = props.theme;
    const url = `url(${theme[props.piece]})`;
    return (
        <div
            className="game-piece"
            style={{
                backgroundImage: url,
            }}
        ></div>
    );
};
