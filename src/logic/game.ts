import { ChessPieceKey, PositionMap } from "../mappings";
import { createTileKey, hashPositionMap, TileInformation } from "./tile";
import { getPieceColor, isKing, isLeftRock, isRightRock, PieceColor } from "./piece";
import * as Move from "./move";
import { MovesChosen } from "./ai";
import { CastleRequirements } from "./move";
import { AIWorker } from "./aiWorker";

const MOVE_SEPARATOR = "\n";

enum GameState {
    WhiteTurn,
    WhiteGivesUp,
    BlackTurn,
    BlackGivesUp,
    StaleMate,
}

export enum GameStatusLabel {
    WhiteMate = "check mate: white wins",
    WhiteCheck = "check on black",
    WhiteTurn = "white turn",
    WhiteSurrender = "white surrendered: black wins",
    BlackMate = "check mate: black wins",
    BlackCheck = "check on white",
    BlackTurn = "black turn",
    BlackSurrender = "black surrendered: white wins",
    StaleMate = "stale mate",
}

interface CastleMoveInformation {
    /**
     * This move was the king's first in the game
     */
    kingFirstMove: boolean;
    /**
     * This move was the first for the left rock
     */
    leftRockFirstMove: boolean;
    /**
     * This move was the first for the right rock
     */
    rightRockFirstMove: boolean;
}

interface GameMove extends Move.MovePositions, CastleMoveInformation {
    positionHash: string;
    pieceMoved: ChessPieceKey;
    pieceTaken: ChessPieceKey | undefined;
}

class MoveStack {
    private m_moves: GameMove[];

    constructor() {
        this.m_moves = [];
    }

    push(move: GameMove) {
        this.m_moves.push(move);
    }

    pop(): GameMove | undefined {
        return this.m_moves.pop();
    }

    private formatMove(from: string, to: string): string {
        return `${from} -> ${to}`;
    }

    toString(): string {
        return this.m_moves
            .map((m) => {
                const from = createTileKey(m.fromRank, m.fromFile);
                const to = createTileKey(m.toRank, m.toFile);
                return this.formatMove(from, to);
            })
            .join(MOVE_SEPARATOR);
    }
    getMovesByColor(color: PieceColor): GameMove[] {
        return this.m_moves.filter((m) => getPieceColor(m.pieceMoved) === color);
    }
}

export interface TurnAnalysis {
    canMove: boolean;
    color: PieceColor;
    castleRequirements: CastleRequirements;
    isCheck: boolean;
}
interface PlayerCastleRequirements {
    white: CastleRequirements;
    black: CastleRequirements;
}

const initialCastleRequirements: CastleRequirements = {
    kingHasMoved: false,
    leftRockMoved: false,
    rightRockMoved: false,
};

export class GameManager {
    private m_state: GameState;
    private m_removedWhitePieces: ChessPieceKey[];
    private m_removedBlackPieces: ChessPieceKey[];
    private m_moves: MoveStack;
    private m_isCheck: boolean;
    private m_isCheckMate: boolean;
    private m_playerCastleRequirements: PlayerCastleRequirements;
    private m_AIWorker: AIWorker;
    private m_movesChosen: MovesChosen;

    constructor() {
        this.m_state = GameState.WhiteTurn;
        this.m_removedWhitePieces = [];
        this.m_removedBlackPieces = [];
        this.m_moves = new MoveStack();
        this.m_isCheck = false;
        this.m_isCheckMate = false;
        this.m_playerCastleRequirements = {
            white: { ...initialCastleRequirements },
            black: { ...initialCastleRequirements },
        };

        this.m_movesChosen = {};
        this.m_AIWorker = new AIWorker();
    }

    private addRemovedPiece(piece: ChessPieceKey | undefined) {
        if (piece !== undefined) {
            const removedPieceColor = getPieceColor(piece);
            switch (removedPieceColor) {
                case PieceColor.Black:
                    this.m_removedBlackPieces.push(piece);
                    break;
                case PieceColor.White:
                    this.m_removedWhitePieces.push(piece);
            }
        }
    }

    private spliceIfFound(from: ChessPieceKey[], item: ChessPieceKey) {
        const index = from.indexOf(item);
        if (index === -1) {
            throw new Error("Item not found. Can't splice!");
        }
        from.splice(index, 1);
    }

    private popRemovedPiece(piece: ChessPieceKey | undefined): ChessPieceKey | undefined {
        if (piece !== undefined) {
            const removedPieceColor = getPieceColor(piece);
            switch (removedPieceColor) {
                case PieceColor.Black:
                    this.spliceIfFound(this.m_removedBlackPieces, piece);
                    break;
                case PieceColor.White:
                    this.spliceIfFound(this.m_removedWhitePieces, piece);
                    break;
            }
        }
        return piece;
    }

    private nextTurn(color: PieceColor, positionMap: PositionMap) {
        switch (this.m_state) {
            case GameState.WhiteTurn:
                if (color === PieceColor.White) {
                    this.m_state = GameState.BlackTurn;
                    this.m_isCheck = Move.isCheck(positionMap, PieceColor.Black);
                    this.m_isCheckMate = this.m_isCheck
                        ? Move.isCheckMate(positionMap, PieceColor.Black, this.m_playerCastleRequirements.black)
                        : this.m_isCheckMate;
                }
                break;
            case GameState.BlackTurn:
                if (color === PieceColor.Black) {
                    this.m_state = GameState.WhiteTurn;
                    this.m_isCheck = Move.isCheck(positionMap, PieceColor.White);
                    this.m_isCheckMate = this.m_isCheck
                        ? Move.isCheckMate(positionMap, PieceColor.White, this.m_playerCastleRequirements.white)
                        : this.m_isCheckMate;
                }
                break;
        }
    }

    private previousTurn(positionMap: PositionMap) {
        switch (this.m_state) {
            case GameState.WhiteTurn:
                this.m_state = GameState.BlackTurn;
                this.m_isCheck = Move.isCheck(positionMap, PieceColor.Black);
                break;
            case GameState.BlackTurn:
                this.m_state = GameState.WhiteTurn;
                this.m_isCheck = Move.isCheck(positionMap, PieceColor.White);
                break;
        }
    }

    private pieceCanBeMoved(piece: ChessPieceKey): boolean {
        const color = getPieceColor(piece);
        switch (this.m_state) {
            case GameState.StaleMate:
                return false;
            case GameState.WhiteGivesUp:
            case GameState.BlackGivesUp:
                return false;
            case GameState.WhiteTurn:
                return color === PieceColor.White;
            case GameState.BlackTurn:
                return color === PieceColor.Black;
        }
    }

    private isInCheck(): boolean {
        return this.m_isCheck;
    }

    private getCastlerequirementsByColor(color: PieceColor): CastleRequirements {
        switch (color) {
            case PieceColor.Black:
                return this.m_playerCastleRequirements.black;
            case PieceColor.White:
                return this.m_playerCastleRequirements.white;
        }
    }

    getMovesString(): string {
        return this.m_moves.toString();
    }

    getColorTurn(): PieceColor {
        switch (this.m_state) {
            case GameState.StaleMate:
                // maybe should return undefined
                return PieceColor.Black;
            case GameState.BlackGivesUp:
            case GameState.BlackTurn:
                return PieceColor.Black;
            case GameState.WhiteGivesUp:
            case GameState.WhiteTurn:
                return PieceColor.White;
        }
    }

    private undoCastle(piece: ChessPieceKey, lastMove: GameMove) {
        const color = getPieceColor(piece);
        const castleRequirements = this.getCastlerequirementsByColor(color);

        if (isKing(piece) && lastMove.kingFirstMove) {
            castleRequirements.kingHasMoved = false;
        } else if (isLeftRock(piece) && lastMove.leftRockFirstMove) {
            castleRequirements.leftRockMoved = false;
        } else if (isRightRock(piece) && lastMove.rightRockFirstMove) {
            castleRequirements.rightRockMoved = false;
        }
    }

    undoMove(positionMap: PositionMap) {
        const lastMove = this.m_moves.pop();
        if (lastMove !== undefined) {
            const endPosition = createTileKey(lastMove.toRank, lastMove.toFile);
            const startPosition = createTileKey(lastMove.fromRank, lastMove.fromFile);
            const movedPiece = positionMap[endPosition];

            if (movedPiece === undefined) {
                // shouldn't happen
                throw new Error(`No piece at ${endPosition}. Can't undo.`);
            }

            // if castle was done, undo it
            this.undoCastle(movedPiece, lastMove);

            Move.movePiece(positionMap, endPosition, startPosition, movedPiece);

            const pieceTaken = this.popRemovedPiece(lastMove.pieceTaken);
            if (pieceTaken !== undefined) {
                positionMap[endPosition] = pieceTaken;
            }
            this.previousTurn(positionMap);
        }
    }

    private updateCastleRequirements(piece: ChessPieceKey, color: PieceColor): CastleMoveInformation {
        const castleRequirements = this.getCastlerequirementsByColor(color);
        const info: CastleMoveInformation = {
            kingFirstMove: false,
            leftRockFirstMove: false,
            rightRockFirstMove: false,
        };

        if (isKing(piece) && !castleRequirements.kingHasMoved) {
            castleRequirements.kingHasMoved = true;
            info.kingFirstMove = true;
        } else if (isLeftRock(piece) && !castleRequirements.leftRockMoved) {
            castleRequirements.leftRockMoved = true;
            info.leftRockFirstMove = true;
        } else if (isRightRock(piece) && !castleRequirements.rightRockMoved) {
            castleRequirements.rightRockMoved = true;
            info.rightRockFirstMove = true;
        }

        return info;
    }

    performMove(positionMap: PositionMap, from: TileInformation, to: TileInformation, piece: ChessPieceKey) {
        const color = getPieceColor(piece);
        const currentKey = createTileKey(to.rank, to.file);
        const previousKey = createTileKey(from.rank, from.file);

        const castleMoveInfo = this.updateCastleRequirements(piece, color);
        const removedPiece = positionMap[currentKey];
        this.addRemovedPiece(removedPiece);

        Move.movePiece(positionMap, previousKey, currentKey, piece);
        const move: GameMove = {
            positionHash: hashPositionMap(positionMap),
            fromFile: from.file,
            fromRank: from.rank,
            toFile: to.file,
            toRank: to.rank,
            pieceMoved: piece,
            pieceTaken: removedPiece,
            ...castleMoveInfo,
        };

        this.m_moves.push(move);
        this.nextTurn(color, positionMap);
    }

    getStatusLabel(): GameStatusLabel {
        switch (this.m_state) {
            case GameState.StaleMate:
                return GameStatusLabel.StaleMate;
            case GameState.WhiteGivesUp:
                if (this.m_isCheckMate) {
                    return GameStatusLabel.BlackMate;
                }
                return GameStatusLabel.WhiteSurrender;
            case GameState.BlackGivesUp:
                if (this.m_isCheckMate) {
                    return GameStatusLabel.WhiteMate;
                }
                return GameStatusLabel.BlackSurrender;
            case GameState.BlackTurn:
                if (this.m_isCheckMate) {
                    return GameStatusLabel.WhiteMate;
                }
                return this.m_isCheck ? GameStatusLabel.WhiteCheck : GameStatusLabel.BlackTurn;
            case GameState.WhiteTurn:
                if (this.m_isCheckMate) {
                    return GameStatusLabel.BlackMate;
                }
                return this.m_isCheck ? GameStatusLabel.BlackCheck : GameStatusLabel.WhiteTurn;
        }
    }

    surrender() {
        const turn = this.getColorTurn();
        switch (turn) {
            case PieceColor.White:
                this.m_state = GameState.WhiteGivesUp;
                break;
            case PieceColor.Black:
                this.m_state = GameState.BlackGivesUp;
                break;
        }
    }

    staleMate() {
        this.m_state = GameState.StaleMate;
    }

    getTurnAnalysis(piece: ChessPieceKey | undefined): TurnAnalysis {
        const color = this.getColorTurn();
        return {
            canMove: piece !== undefined && this.pieceCanBeMoved(piece),
            color,
            isCheck: this.isInCheck(),
            castleRequirements: this.getCastlerequirementsByColor(color),
        };
    }

    isInCheckMate(): boolean {
        return this.m_isCheckMate;
    }

    async AIMove(
        positionMap: PositionMap,
        success: (from: string, to: string) => void,
        checkMate: () => void,
        noMoves: () => void
    ) {
        const check = this.m_isCheck;
        const colorTurn = this.getColorTurn();
        const currentCastleReq = this.getCastlerequirementsByColor(colorTurn);
        const previousMoves = this.m_moves.getMovesByColor(colorTurn).map((m) => m.positionHash);
        const nextMove = await this.m_AIWorker.getNextMove(positionMap, colorTurn, currentCastleReq, previousMoves);
        this.m_movesChosen = nextMove.moves;
        if (nextMove.from === undefined || nextMove.to === undefined) {
            if (check) {
                checkMate();
            } else {
                noMoves();
            }
        } else {
            success(nextMove.from, nextMove.to);
        }
    }

    getLastMovesChosen(): MovesChosen {
        return this.m_movesChosen;
    }

    sleep(seconds: number): Promise<void> {
        return new Promise<void>((resolve) => {
            setTimeout(() => {
                resolve();
            }, seconds * 1000);
        });
    }

    startNewGame() {
        this.m_state = GameState.WhiteTurn;
        this.m_removedWhitePieces = [];
        this.m_removedBlackPieces = [];
        this.m_moves = new MoveStack();
        this.m_isCheck = false;
        this.m_isCheckMate = false;
        this.m_playerCastleRequirements = {
            white: { ...initialCastleRequirements },
            black: { ...initialCastleRequirements },
        };
        this.m_movesChosen = {};
        this.m_AIWorker = new AIWorker();
    }
}
