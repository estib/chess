import * as Move from "./move";
import { ChessPieceKey, PositionMap } from "../mappings";
import { getPieceColor, PieceColor, getPieceValue, getContraryColor, isKing, isPawn } from "./piece";
import { CastleRequirements } from "./move";
import { distanceBetween, hashPositionMap } from "./tile";

const DEFAULT_DIVE_DEPTH = 3;
const MAX_DIVE_DEPTH = 10;
const MAX_REPEATED_POSITION = 2;
const PROMOTION_SORT_VALUE = 9;

// VALUES
const MAX_VALUE = Number.MAX_SAFE_INTEGER;
const MIN_VALUE = Number.MIN_SAFE_INTEGER;

const PIECE_VALUE = 1000;
const CENTER_TILE_COVERAGE_VALUE = 350;
const TILE_COVERAGE_VALUE = 50;
const REPEATED_POSITION_VALUE = 800;
const THREATENED_PIECE_VALUE = 5;
const THREATENED_KING_PENALTY = 50;
const OPPONENT_THREATENED_KING_BONUS = 100;
const THREATENED_PIECE_UNPROTECTED_VALUE = 1200;
const KING_ENDGAME_PROXIMITY = 50;

interface PositionEvaluationResult {
    score: number;
    timeTaken: number;
    fromCache: boolean;
}

interface EvaluationCache {
    white: {
        [positionHash: string]: number;
    };
    black: {
        [positionHash: string]: number;
    };
}

interface PositionEvaluatorOptions {
    checkUnprotectedPieces?: boolean;
    checkRepeatedPosition?: boolean;
    checkCenterTileCoverage?: boolean;
}

class PositionEvaluator {
    private m_pieceValue: number;
    private m_centerTileInfluenceValue: number;
    private m_repeatedPositionValue: number;
    private m_influenceOverPieceValue: number;
    private m_influenceOverUnprotectedPieceValue: number;
    private m_influenceOverTileValue: number;
    private m_kingProximityValue: number;
    private m_kingThreatenedPenalty: number;
    private m_kingThreatenedBonus: number;
    // options
    private m_checkUnprotectedPieces: boolean;
    private m_checkRepeatedPosition: boolean;
    private m_checkCenterTileCoverage: boolean;
    // cache
    private m_cachedEvaluations: EvaluationCache;

    constructor(options: PositionEvaluatorOptions) {
        this.m_pieceValue = PIECE_VALUE;
        this.m_centerTileInfluenceValue = CENTER_TILE_COVERAGE_VALUE;
        this.m_repeatedPositionValue = REPEATED_POSITION_VALUE;
        this.m_influenceOverPieceValue = THREATENED_PIECE_VALUE;
        this.m_influenceOverUnprotectedPieceValue = THREATENED_PIECE_UNPROTECTED_VALUE;
        this.m_influenceOverTileValue = TILE_COVERAGE_VALUE;
        this.m_kingProximityValue = KING_ENDGAME_PROXIMITY;
        this.m_kingThreatenedPenalty = THREATENED_KING_PENALTY;
        this.m_kingThreatenedBonus = OPPONENT_THREATENED_KING_BONUS;
        // options
        this.m_checkUnprotectedPieces = !!options?.checkUnprotectedPieces;
        this.m_checkRepeatedPosition = !!options?.checkRepeatedPosition;
        this.m_checkCenterTileCoverage = !!options?.checkCenterTileCoverage;
        // cache
        this.m_cachedEvaluations = { white: {}, black: {} };
    }

    /**
     * Returns negative value according to the amount of repeated times the current position has benn previously found
     */
    private repeatedPosition(currentPosition: string, previousPositions: string[]): number {
        if (!this.m_checkRepeatedPosition) {
            return 0;
        }
        const repeatedPosition = Move.timesRepeatedPosition(previousPositions, currentPosition);
        if (repeatedPosition > 5) {
            console.log("Too many times repeated");
        }
        return -(this.m_repeatedPositionValue * repeatedPosition);
    }

    /**
     * Returns the value according to the value of the piece times the amounf of center tiles covered
     */
    private centerCoverage(piece: ChessPieceKey, tileKey: string): number {
        if (!this.m_checkCenterTileCoverage || isKing(piece)) {
            return 0;
        }
        return this.m_centerTileInfluenceValue * Move.amountOfCenterTilesCovered(piece, tileKey);
    }

    /**
     * Returns the negative value according to the values of all unprotected pieces
     */
    private unprotectedPieces(playerPieces: PositionMap, protectedPieces: PositionMap): number {
        if (!this.m_checkUnprotectedPieces) {
            return 0;
        }
        let score: number = 0;
        Move.iterateOverPieces(playerPieces, (p: ChessPieceKey, tileKey: string) => {
            if (protectedPieces[tileKey] === undefined) {
                // threatened and unprotected
                const value = getPieceValue(p);
                score -= this.m_influenceOverUnprotectedPieceValue * value;
            }
        });
        return score;
    }

    /**
     * Return the value of development of a piece position
     *
     * Number of tiles covered for non-king pieces
     */
    private pieceDevelopment(piece: ChessPieceKey, tileKey: string): number {
        if (isKing(piece)) {
            return 0;
        }
        return Move.amountOfTilesCovered(piece, tileKey) * this.m_influenceOverTileValue;
    }

    /**
     * Closen the king to the kings opponent when endgame starts
     *
     * Returns a negative number corresponding to the distance between kings. Objective is to incentivize
     * that the king follows the other king
     */
    private kingEndGame(
        opponentPiecesCount: number,
        kingTileKey: string | undefined,
        opponentKingTileKey: string | undefined
    ): number {
        if (kingTileKey !== undefined && opponentKingTileKey !== undefined) {
            const endGameFactor = 16 - opponentPiecesCount;
            const kingDistance = distanceBetween(kingTileKey, opponentKingTileKey);
            return endGameFactor * kingDistance * -this.m_kingProximityValue;
        }
        return 0;
    }

    /**
     * Get cached position score
     */
    private getCachedPosition(positionHash: string, colorTurn: PieceColor): number | undefined {
        switch (colorTurn) {
            case PieceColor.Black:
                return this.m_cachedEvaluations.black[positionHash];
            case PieceColor.White:
                return this.m_cachedEvaluations.white[positionHash];
        }
    }

    /**
     * Cache position score
     */
    private cachePosition(positionHash: string, colorTurn: PieceColor, score: number) {
        switch (colorTurn) {
            case PieceColor.Black:
                this.m_cachedEvaluations.black[positionHash] = score;
                break;
            case PieceColor.White:
                this.m_cachedEvaluations.white[positionHash] = score;
                break;
        }
    }

    getCurrentScore(
        positionMap: PositionMap,
        colorTurn: PieceColor,
        previousPositions: string[]
    ): PositionEvaluationResult {
        let score: number = 0;

        const positionHash = hashPositionMap(positionMap);
        // check cache
        const maybeCachedScore = this.getCachedPosition(positionHash, colorTurn);
        if (maybeCachedScore !== undefined) {
            return {
                score: maybeCachedScore,
                timeTaken: 0,
                fromCache: true,
            };
        }

        const playerPieces: PositionMap = {};
        const protectedPieces: PositionMap = {};
        // repeated move
        score += this.repeatedPosition(positionHash, previousPositions);

        let opponentPiecesCount: number = 0;
        let kingPosition: string | undefined = undefined;
        let opponentKingPosition: string | undefined = undefined;

        Move.iterateOverPieces(positionMap, (piece: ChessPieceKey, tileKey: string) => {
            const pieceColor = getPieceColor(piece);
            const pieceValue = getPieceValue(piece);
            if (pieceColor === colorTurn) {
                playerPieces[tileKey] = piece;
                if (isKing(piece)) {
                    kingPosition = tileKey;
                }

                score += pieceValue * this.m_pieceValue;
                // piece development
                score += this.pieceDevelopment(piece, tileKey);
                // check center tile coverage
                score += this.centerCoverage(piece, tileKey);
                if (!isKing(piece)) {
                    Move.iteratePiecesThreatened(piece, tileKey, positionMap, (p: ChessPieceKey, tK: string) => {
                        const kingBonus = isKing(p) ? this.m_kingThreatenedBonus : 0;
                        const threatenedPieceValue = getPieceValue(p) + kingBonus;
                        const valueDelta = threatenedPieceValue - pieceValue;
                        const value = valueDelta >= 0 ? valueDelta : 1;
                        score += this.m_influenceOverPieceValue * value;
                    });
                }

                // protected pieces
                if (this.m_checkUnprotectedPieces) {
                    Move.iteratePiecesProtected(piece, tileKey, positionMap, (p: ChessPieceKey, tK: string) => {
                        protectedPieces[tK] = p;
                    });
                }
            } else {
                opponentPiecesCount++;
                if (isKing(piece)) {
                    opponentKingPosition = tileKey;
                }
                score -= pieceValue * this.m_pieceValue;
                // piece development
                score -= this.pieceDevelopment(piece, tileKey);
                // check center tile coverage
                score -= this.centerCoverage(piece, tileKey);
                if (!isKing(piece)) {
                    Move.iteratePiecesThreatened(piece, tileKey, positionMap, (p: ChessPieceKey, tK: string) => {
                        const kingBonus = isKing(p) ? this.m_kingThreatenedPenalty : 0;
                        const threatenedPieceValue = getPieceValue(p) + kingBonus;
                        const valueDelta = threatenedPieceValue - pieceValue;
                        const value = valueDelta >= 0 ? valueDelta : 1;
                        score -= this.m_influenceOverPieceValue * value;
                    });
                }
            }
        });
        // king proximity on endgame
        score += this.kingEndGame(opponentPiecesCount, kingPosition, opponentKingPosition);

        score += this.unprotectedPieces(playerPieces, protectedPieces);
        this.cachePosition(positionHash, colorTurn, score);
        return {
            score,
            timeTaken: 0,
            fromCache: false,
        };
    }
}

export interface MovesChosen {
    [index: string]: PositionMap;
}

function moveKeyFromDepth(depth: number): string {
    return depth.toString();
}

function addChosenMove(moves: MovesChosen, positionMap: PositionMap, depth: number): MovesChosen {
    const key = moveKeyFromDepth(depth);
    return { ...moves, [key]: positionMap };
}

export interface BranchResult {
    from: string | undefined;
    to: string | undefined;
    value: number;
    moves: MovesChosen;
}

function initBranchResult(value: number): BranchResult {
    return {
        from: undefined,
        to: undefined,
        value,
        moves: {},
    };
}

interface BranchParams {
    castleRequirements: CastleRequirements;
    positionMap: PositionMap;
    colorTurn: PieceColor;
    previousPositions: string[];
    stepsLeft: number;
    depth: number;
    alpha: number;
    beta: number;
}

function sortMoves(moveA: Move.HypoteticalMove, moveB: Move.HypoteticalMove): number {
    const pieceA = moveA.piece;
    const pieceB = moveB.piece;
    const valueA = getPieceValue(pieceA);
    const valueB = getPieceValue(pieceB);

    const valueTakenA = moveA.pieceTaken !== undefined ? getPieceValue(moveA.pieceTaken) : 0;
    const valueTakenB = moveB.pieceTaken !== undefined ? getPieceValue(moveB.pieceTaken) : 0;
    // pawn promotion
    const promotionA = isPawn(pieceA) ? PROMOTION_SORT_VALUE : 0;
    const promotionB = isPawn(pieceB) ? PROMOTION_SORT_VALUE : 0;

    const totalA = promotionA + valueA + valueTakenA;
    const totalB = promotionB + valueB + valueTakenB;

    return totalB - totalA;
}

function ignoreMove(hMove: Move.HypoteticalMove, previousPositions: string[]): boolean {
    const positionHash = hashPositionMap(hMove.nextPositionMap);
    const repeatedPosition = Move.timesRepeatedPosition(previousPositions, positionHash);
    if (repeatedPosition >= MAX_REPEATED_POSITION) {
        return true;
    }
    return false;
}

export class GameAI {
    private m_moveDiveDepth: number;
    private m_maxMoveDiveDepth: number;
    private m_maxBranchDepthReached: number;
    private m_shufflePieceIteration: boolean;
    private m_extendBranches: boolean;
    private m_evaluator: PositionEvaluator;

    private m_evaluationNumber: number;
    private m_cachedEvaluationNumber: number;
    private m_evaluationDurations: number;
    private m_extendeBranchNumber: number;

    constructor(diveDepth?: number) {
        this.m_evaluationNumber = 0;
        this.m_evaluationDurations = 0;
        this.m_extendeBranchNumber = 0;
        this.m_cachedEvaluationNumber = 0;
        this.m_moveDiveDepth = diveDepth || DEFAULT_DIVE_DEPTH;
        this.m_maxMoveDiveDepth = MAX_DIVE_DEPTH;
        this.m_shufflePieceIteration = true;
        this.m_extendBranches = true;
        this.m_maxBranchDepthReached = 0;
        this.m_evaluator = new PositionEvaluator({
            checkCenterTileCoverage: true,
            checkRepeatedPosition: true,
            checkUnprotectedPieces: true,
        });
    }

    private nextBranchParams(oldParams: BranchParams, nextPositionMap: PositionMap, extend: boolean): BranchParams {
        const newPositionHash = hashPositionMap(nextPositionMap);
        const isLeaveNode = oldParams.stepsLeft === 1; // extend only end of branch;
        const hasNotReachedMaxDepth = oldParams.depth < this.m_maxMoveDiveDepth;
        const shouldExtend = this.m_extendBranches && hasNotReachedMaxDepth && extend && isLeaveNode;

        const stepsLeft = shouldExtend ? oldParams.stepsLeft : oldParams.stepsLeft - 1;
        const depth = oldParams.depth + 1;

        if (shouldExtend) {
            this.m_extendeBranchNumber++;
            if (this.m_maxBranchDepthReached < depth) {
                this.m_maxBranchDepthReached = depth;
            }
        }
        return {
            ...oldParams,
            positionMap: nextPositionMap,
            previousPositions: [...oldParams.previousPositions, newPositionHash],
            stepsLeft,
            depth,
        };
    }

    private shouldExtendBranch(nextTileKey: string, positionMap: PositionMap): boolean {
        return positionMap[nextTileKey] !== undefined;
    }

    private getCurrentScore(params: BranchParams): number {
        this.m_evaluationNumber++;
        const { score, timeTaken, fromCache } = this.m_evaluator.getCurrentScore(
            params.positionMap,
            params.colorTurn,
            params.previousPositions
        );
        this.m_evaluationDurations += timeTaken;
        if (fromCache) {
            this.m_cachedEvaluationNumber++;
        }
        return score;
    }

    private maximizeAlphaBetaPruning(bp: BranchParams): BranchResult {
        const branchResult = initBranchResult(MIN_VALUE);
        const sfl = this.m_shufflePieceIteration;
        Move.iterateAllMovesForColor(
            bp.positionMap,
            bp.colorTurn,
            bp.castleRequirements,
            sfl,
            (h: Move.HypoteticalMove) => {
                const shouldExtend = this.shouldExtendBranch(h.to, bp.positionMap);
                const nextBP: BranchParams = this.nextBranchParams(bp, h.nextPositionMap, shouldExtend);
                const { value, moves } = this.minimaxAlphaBetaPruning(nextBP, false);
                if (value >= branchResult.value) {
                    branchResult.from = h.from;
                    branchResult.to = h.to;
                    branchResult.value = value;
                    branchResult.moves = addChosenMove(moves, h.nextPositionMap, bp.depth);
                }
                bp.alpha = Math.max(bp.alpha, value);
                if (bp.beta <= bp.alpha) {
                    return false;
                }
                return true;
            },
            (h) => ignoreMove(h, bp.previousPositions),
            sortMoves
        );
        return branchResult;
    }

    private minimizeAlphaBetaPruning(bp: BranchParams): BranchResult {
        const branchResult = initBranchResult(MAX_VALUE);
        const color = getContraryColor(bp.colorTurn);
        const sfl = this.m_shufflePieceIteration;
        Move.iterateAllMovesForColor(
            bp.positionMap,
            color,
            bp.castleRequirements,
            sfl,
            (h: Move.HypoteticalMove) => {
                const shouldExtend = this.shouldExtendBranch(h.to, bp.positionMap);
                const nextBP: BranchParams = this.nextBranchParams(bp, h.nextPositionMap, shouldExtend);
                const { value, moves } = this.minimaxAlphaBetaPruning(nextBP, true);
                if (value <= branchResult.value) {
                    branchResult.from = h.from;
                    branchResult.to = h.to;
                    branchResult.value = value;
                    branchResult.moves = addChosenMove(moves, h.nextPositionMap, bp.depth);
                }
                bp.beta = Math.min(bp.beta, value);
                if (bp.beta <= bp.alpha) {
                    return false;
                }
                return true;
            },
            (h) => ignoreMove(h, bp.previousPositions),
            sortMoves
        );
        return branchResult;
    }

    private minimaxAlphaBetaPruning(params: BranchParams, maximizingPlayer: boolean = true): BranchResult {
        if (params.stepsLeft === 0) {
            const v = this.getCurrentScore(params);
            return initBranchResult(v);
        }
        if (maximizingPlayer) {
            return this.maximizeAlphaBetaPruning(params);
        }
        return this.minimizeAlphaBetaPruning(params);
    }

    getNextMove(
        positionMap: PositionMap,
        colorTurn: PieceColor,
        castleRequirements: CastleRequirements,
        previousPositions: string[]
    ): BranchResult {
        this.m_evaluationNumber = 0;
        this.m_evaluationDurations = 0;
        this.m_extendeBranchNumber = 0;
        this.m_cachedEvaluationNumber = 0;
        this.m_maxBranchDepthReached = 0;
        console.log(`\n\nMove ${colorTurn}: ${previousPositions.length + 1}`);

        const params: BranchParams = {
            castleRequirements,
            positionMap,
            colorTurn,
            previousPositions,
            stepsLeft: this.m_moveDiveDepth,
            depth: 0,
            alpha: MIN_VALUE,
            beta: MAX_VALUE,
        };
        const result = this.minimaxAlphaBetaPruning(params);

        console.log(
            `Evaluations: ${this.m_evaluationNumber}
Max evaluation depth: ${this.m_maxBranchDepthReached}
Cached evaluations: ${this.m_cachedEvaluationNumber}
Extended branch number: ${this.m_extendeBranchNumber}
Average evaluation duration: ${this.m_evaluationDurations / this.m_evaluationNumber}`
        );
        return result;
    }
}
