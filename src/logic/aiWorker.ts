import { PositionMap } from "../mappings";
import { BranchResult } from "./ai";
import { CastleRequirements } from "./move";
import { PieceColor } from "./piece";

const WORKER_URL = "worker.js";

export enum AIWorkerMessageType {
    GetNextMove = "get-next-move",
}

export interface BaseAIWorkerMessageParameters {
    type: AIWorkerMessageType;
}

export interface GetNextMoveMessageParameters extends BaseAIWorkerMessageParameters {
    type: AIWorkerMessageType.GetNextMove;
    positionMap: PositionMap;
    colorTurn: PieceColor;
    castleRequirements: CastleRequirements;
    previousPositions: string[];
}

export type AIWorkerMessageParameters = GetNextMoveMessageParameters;

// #########################################################################################################

export interface BaseAIWorkerResponse {
    type: AIWorkerMessageType;
}

export interface GetNextMoveResponse extends BaseAIWorkerResponse {
    type: AIWorkerMessageType.GetNextMove;
    nextMove: BranchResult;
}

export type AIWorkerResponse = GetNextMoveResponse;

// #########################################################################################################

export type GetNextMoveCallback = (nextMove: BranchResult) => void;

// #########################################################################################################

export class AIWorker {
    private m_Worker: Worker;
    private m_blockedTransactions: AIWorkerMessageType[];

    private m_getNextMoveCallback: GetNextMoveCallback | undefined = undefined;

    constructor() {
        this.m_Worker = new Worker(WORKER_URL);
        this.m_blockedTransactions = [];

        this.m_Worker.onmessage = (e) => {
            const response = e.data as AIWorkerResponse;
            switch (response.type) {
                case AIWorkerMessageType.GetNextMove: {
                    this.m_getNextMoveCallback?.(response.nextMove);
                    this.m_blockedTransactions = this.m_blockedTransactions.filter((t) => t !== response.type);
                }
            }
        };
    }

    getNextMove(
        positionMap: PositionMap,
        colorTurn: PieceColor,
        castleRequirements: CastleRequirements,
        previousPositions: string[]
    ): Promise<BranchResult> {
        return new Promise((resolve) => {
            const type = AIWorkerMessageType.GetNextMove;
            if (this.m_blockedTransactions.includes(type)) {
                // don't do anything
                return;
            }
            this.m_Worker.postMessage({ type, positionMap, colorTurn, castleRequirements, previousPositions });
            this.m_getNextMoveCallback = resolve;
            this.m_blockedTransactions.push(type);
        });
    }
}
