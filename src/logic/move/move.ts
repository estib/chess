import * as _ from "lodash";
import * as Validation from "./validation";
import { getCurrentPossibleMoves } from "./possible";
import * as Tile from "../tile";
import * as Piece from "../piece";
import { PositionMap, ChessPieceKey, possibleMovesMap, possibleMoveDirectionsMap } from "../../mappings";

function shuffleArray<T>(array: T[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

// Possible

export interface CastleRequirements {
    kingHasMoved: boolean;
    leftRockMoved: boolean;
    rightRockMoved: boolean;
}

interface CastleDetermination {
    valid: boolean;
    fileDifference: number;
}

export function iterateOverPieces(
    positionMap: PositionMap,
    callback: (piece: ChessPieceKey, tileKey: string, totalPieces: number) => void
) {
    const occupiedTiles = Tile.getOccupiedTiles(positionMap);
    for (let i = 0; i < occupiedTiles.length; i++) {
        const tileKey = occupiedTiles[i];
        const piece = positionMap[tileKey];
        callback(piece, tileKey, occupiedTiles.length);
    }
}

export function iterateOverPiecesOfColor(
    positionMap: PositionMap,
    color: Piece.PieceColor,
    shuffle: boolean,
    callback: (piece: ChessPieceKey, tileKey: string) => void,
    sorter?: (tileA: string, tileB: string) => number
) {
    const occupiedTilesOfColor = Tile.getOccupiedTilesOfColor(positionMap, color);
    if (shuffle) {
        shuffleArray(occupiedTilesOfColor);
    }
    if (sorter !== undefined) {
        occupiedTilesOfColor.sort(sorter);
    }
    for (let i = 0; i < occupiedTilesOfColor.length; i++) {
        const tileKey = occupiedTilesOfColor[i];
        const piece = positionMap[tileKey];
        callback(piece, tileKey);
    }
}

export function iterateOverPossibleMoves(
    piece: ChessPieceKey,
    tileKey: string,
    positionMap: PositionMap,
    castleRequirements: CastleRequirements,
    callback: (nextTileKey: string, nextPositionMap: PositionMap) => boolean,
    sorter?: (tileA: string, tileB: string) => number
) {
    const possibleMoves = getCurrentPossibleMoves(tileKey, positionMap, castleRequirements);

    if (sorter !== undefined) {
        possibleMoves.sort(sorter);
    }

    for (let j = 0; j < possibleMoves.length; j++) {
        const nextTileKey = possibleMoves[j];
        const nextPositionMap = movePieceCopy(positionMap, tileKey, nextTileKey, piece);
        const canContinue = callback(nextTileKey, nextPositionMap);
        if (!canContinue) {
            break;
        }
    }
}

export interface PossibleAttack {
    targetTileKey: string;
    targetPiece: ChessPieceKey;
    value: number;
}

export type AttackDictionary = { [tileKey: string]: PossibleAttack[] };

/**
 * Iterates over all possible attacks for a given piece.
 */
export function iterateOverPossibleAttacks(
    piece: ChessPieceKey,
    tileKey: string,
    positionMap: PositionMap,
    castleRequirements: CastleRequirements,
    callback: (attack: PossibleAttack) => void
) {
    // no need to check if target piece is of
    // opponent's color if king is checked
    // that is already taken into account in possible moves
    iterateOverPossibleMoves(piece, tileKey, positionMap, castleRequirements, (targetTileKey: string) => {
        const targetPiece = positionMap[targetTileKey];
        if (targetPiece !== undefined) {
            callback({
                targetPiece,
                targetTileKey,
                value: Piece.getPieceValue(targetPiece),
            });
        }
        return true;
    });
}

// Validation

export function isCheckMate(
    positionMap: PositionMap,
    kingColor: Piece.PieceColor,
    castleRequirements: CastleRequirements,
    isSimulation?: boolean
): boolean {
    const occupiedTiles = Tile.getOccupiedTiles(positionMap);
    for (let i = 0; i < occupiedTiles.length; i++) {
        const tileKey = occupiedTiles[i];
        const piece = positionMap[tileKey];
        const color = Piece.getPieceColor(piece);
        if (color === kingColor) {
            const allPossibleMoves = getCurrentPossibleMoves(tileKey, positionMap, castleRequirements);
            for (let j = 0; j < allPossibleMoves.length; j++) {
                const possibleTile = allPossibleMoves[j];
                const nextPositionMap = movePieceCopy(positionMap, tileKey, possibleTile, piece);
                if (!Validation.isCheck(nextPositionMap, kingColor, isSimulation)) {
                    return false;
                }
            }
        }
    }
    return true;
}

export interface MoveDeterminationParams {
    previousTile: Tile.TileInformation;
    currentTile: Tile.TileInformation;
    positionMap: PositionMap;
    castleRequirements: CastleRequirements;
    isInCheck: boolean;
    isCastle: boolean;
    color: Piece.PieceColor;
}

export function canMovePieceToTile(params: MoveDeterminationParams): boolean {
    // no piece. shouldn't happen
    if (params.previousTile.piece === undefined) {
        return false;
    }

    // can't kill same-color pieces
    if (Piece.sameColorPieces(params.previousTile.piece, params.currentTile.piece)) {
        return false;
    }

    const previousKey = Tile.createTileKey(params.previousTile.rank, params.previousTile.file);
    const currentKey = Tile.createTileKey(params.currentTile.rank, params.currentTile.file);

    // same tile
    if (previousKey === currentKey) {
        return false;
    }

    const moveParams: Validation.MovementValidationParams = {
        fromRank: params.previousTile.rank,
        fromFile: params.previousTile.file,
        toRank: params.currentTile.rank,
        toFile: params.currentTile.file,
        piece: params.previousTile.piece,
        attack: params.currentTile.piece !== undefined,
        positionMap: params.positionMap,
        isCastle: params.isCastle,
    };

    const possibleMoves = getCurrentPossibleMoves(previousKey, params.positionMap, params.castleRequirements);
    const validMove = Validation.validateMove(moveParams) && possibleMoves.includes(currentKey);

    if (params.isInCheck && validMove) {
        const postionsCopy: PositionMap = { ...params.positionMap };
        movePiece(postionsCopy, previousKey, currentKey, params.previousTile.piece);
        return !Validation.isCheck(postionsCopy, params.color);
    }

    return validMove;
}

function determineCastle(
    fromFile: Tile.TileFile,
    toFile: Tile.TileFile,
    piece: ChessPieceKey,
    previousKey: string
): CastleDetermination {
    const start = Tile.getTileFileIndex(fromFile);
    const end = Tile.getTileFileIndex(toFile);
    const fileDifference = start - end;
    const fileDelta = Math.abs(fileDifference);
    const valid =
        fileDelta === Validation.KING_CASTLE_MAX_STEPS &&
        Piece.isKing(piece) &&
        Piece.isInitialPosition(piece, previousKey);
    return { valid, fileDifference };
}

export function isCastleMove(previousTile: Tile.TileInformation, currentTile: Tile.TileInformation): boolean {
    // no piece. shouldn't happen
    if (previousTile.piece === undefined) {
        return false;
    }
    const previousKey = Tile.createTileKey(previousTile.rank, previousTile.file);
    return determineCastle(previousTile.file, currentTile.file, previousTile.piece, previousKey).valid;
}

function performCastle(piece: ChessPieceKey, fileDifference: number, positionMap: PositionMap) {
    const rock = Piece.getCastlingRock(piece, fileDifference);
    const currentRockTile = Tile.getTileOfPiece(positionMap, rock);
    const castleTile = Piece.getCastlingTile(piece, fileDifference);
    const pieceInTheWay = positionMap[castleTile] !== undefined;
    if (!pieceInTheWay && currentRockTile !== undefined) {
        movePiece(positionMap, currentRockTile, castleTile, rock);
    }
}

function isPromotion(piece: ChessPieceKey, tileKey: string, color: Piece.PieceColor): boolean {
    if (Piece.isPawn(piece)) {
        return Piece.isPromotionRank(tileKey, color);
    }
    return false;
}

export function movePiece(positionMap: PositionMap, from: string, to: string, piece: ChessPieceKey) {
    let pieceToMove: ChessPieceKey = piece;
    const color = Piece.getPieceColor(piece);
    const fromFile = Tile.getFileFromKey(from);
    const toFile = Tile.getFileFromKey(to);
    const { valid, fileDifference } = determineCastle(fromFile, toFile, piece, from);
    if (valid) {
        performCastle(piece, fileDifference, positionMap);
    } else if (isPromotion(piece, to, color)) {
        pieceToMove = Piece.getQueenOfColor(color);
    }
    delete positionMap[from];
    positionMap[to] = pieceToMove;
}

export function movePieceCopy(positionMap: PositionMap, from: string, to: string, piece: ChessPieceKey): PositionMap {
    const nextPositionMap: PositionMap = _.cloneDeep(positionMap);
    movePiece(nextPositionMap, from, to, piece);
    return nextPositionMap;
}

export const centerTiles: string[] = ["D4", "D5", "E4", "E5"];

function getAttackingMovesFromMap(piece: ChessPieceKey, tileKey: string): string[] {
    if (Piece.isPawn(piece)) {
        const pawnMoves = possibleMoveDirectionsMap[piece][tileKey];
        const moves: string[] = [];

        const conditionallyAdd = (m: string[] | undefined) => {
            if (m !== undefined) {
                moves.push(...m);
            }
        };
        [pawnMoves.backwardLeft, pawnMoves.backwardRight, pawnMoves.forwardLeft, pawnMoves.forwardRight].forEach(
            conditionallyAdd
        );

        return moves;
    }
    return possibleMovesMap[piece][tileKey];
}

function iteratePossibleMoveMap(
    piece: ChessPieceKey,
    tileKey: string,
    attackOnly: boolean,
    callback: (possibleTile: string) => boolean
) {
    const possibleMoves = attackOnly ? getAttackingMovesFromMap(piece, tileKey) : possibleMovesMap[piece][tileKey];
    for (let i = 0; i < possibleMoves.length; i++) {
        const possibleTile = possibleMoves[i];
        const canContinue = callback(possibleTile);
        if (!canContinue) {
            break;
        }
    }
}

export function amountOfCenterTilesCovered(piece: ChessPieceKey, tileKey: string): number {
    let covers: number = 0;
    iteratePossibleMoveMap(piece, tileKey, false, (possibleTile) => {
        if (centerTiles.includes(possibleTile)) {
            covers++;
        }
        return true;
    });
    return covers;
}

export function amountOfTilesCovered(piece: ChessPieceKey, tileKey: string): number {
    return possibleMovesMap[piece][tileKey].length;
}

/**
 * Return the number of times a given position is found in the previous positions
 */
export function timesRepeatedPosition(previousPositions: string[], currentPosition: string): number {
    let timesPositionFound = 0;
    for (let i = 0; i < previousPositions.length; i++) {
        const pp = previousPositions[i];
        if (pp === currentPosition) {
            timesPositionFound++;
        }
    }
    return timesPositionFound;
}

/**
 * Iterate over all opponent pieces influenced over
 */
export function iteratePiecesThreatened(
    piece: ChessPieceKey,
    tileKey: string,
    positionMap: PositionMap,
    callback: (piece: ChessPieceKey, tileKey: string) => void
) {
    const colorTurn = Piece.getPieceColor(piece);
    iteratePossibleMoveMap(piece, tileKey, true, (possibleTile) => {
        const targetPiece = positionMap[possibleTile];
        if (targetPiece !== undefined) {
            const color = Piece.getPieceColor(targetPiece);
            if (colorTurn !== color) {
                callback(targetPiece, possibleTile);
            }
        }
        return true;
    });
}

/**
 * Iterate over all player pieces influenced over
 */
export function iteratePiecesProtected(
    piece: ChessPieceKey,
    tileKey: string,
    positionMap: PositionMap,
    callback: (piece: ChessPieceKey, tileKey: string) => void
) {
    const colorTurn = Piece.getPieceColor(piece);
    iteratePossibleMoveMap(piece, tileKey, true, (possibleTile) => {
        const targetPiece = positionMap[possibleTile];
        if (targetPiece !== undefined) {
            const color = Piece.getPieceColor(targetPiece);
            if (colorTurn === color) {
                callback(targetPiece, possibleTile);
            }
        }
        return true;
    });
}

export interface MoveDirections {
    forward?: string[];
    backward?: string[];
    left?: string[];
    right?: string[];
    forwardLeft?: string[];
    forwardRight?: string[];
    backwardLeft?: string[];
    backwardRight?: string[];
}

export interface HypoteticalMove {
    piece: ChessPieceKey;
    pieceTaken: ChessPieceKey | undefined;
    from: string;
    to: string;
    nextPositionMap: PositionMap;
}

/**
 * Iterate over all moves for color in a position
 */

export function iterateAllMovesForColor(
    positionMap: PositionMap,
    color: Piece.PieceColor,
    castleRequirements: CastleRequirements,
    preshuffle: boolean,
    callback: (hMove: HypoteticalMove) => boolean,
    shouldIgnore: (hMove: HypoteticalMove) => boolean,
    sorter?: (moveA: HypoteticalMove, moveB: HypoteticalMove) => number
) {
    const hypoteticalMoves: HypoteticalMove[] = [];
    iterateOverPiecesOfColor(positionMap, color, false, (piece: ChessPieceKey, tileKey: string) => {
        iterateOverPossibleMoves(
            piece,
            tileKey,
            positionMap,
            castleRequirements,
            (nextTileKey: string, nextPM: PositionMap) => {
                const hMove: HypoteticalMove = {
                    piece,
                    from: tileKey,
                    to: nextTileKey,
                    nextPositionMap: nextPM,
                    pieceTaken: positionMap[nextTileKey],
                };
                if (shouldIgnore(hMove)) {
                    return true;
                }
                hypoteticalMoves.push(hMove);
                return true;
            }
        );
    });

    if (preshuffle) {
        shuffleArray(hypoteticalMoves);
    }

    if (sorter !== undefined) {
        hypoteticalMoves.sort(sorter);
    }

    for (let i = 0; i < hypoteticalMoves.length; i++) {
        const hMove = hypoteticalMoves[i];
        const canContinue = callback(hMove);
        if (!canContinue) {
            break;
        }
    }
}
