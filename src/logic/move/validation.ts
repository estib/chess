import * as Tile from "../tile";
import * as Piece from "../piece";
import { PositionMap, ChessPieceKey } from "../../mappings";

export const KING_MAX_STEPS = 1;
export const KING_CASTLE_MAX_STEPS = 2;
export const PAWN_MAX_STEPS = 1;
export const PAWN_MAX_STEPS_INITIAL = 2;
export const MAX_STEPS = 8;

export interface MovePositions {
    fromRank: Tile.TileRank;
    fromFile: Tile.TileFile;
    toRank: Tile.TileRank;
    toFile: Tile.TileFile;
}

function normalize(delta: number): number {
    return delta > 0 ? 1 : -1;
}

/**
 * Check if a move is diagonal.
 */
function isDiagonalMove(params: MovePositions): boolean {
    const { fromRank, fromFile, toRank, toFile } = params;

    const startFile = Tile.getTileFileIndex(fromFile);
    const endFile = Tile.getTileFileIndex(toFile);

    const startRank = Tile.getTileRankIndex(fromRank);
    const endRank = Tile.getTileRankIndex(toRank);

    const deltaFile = Math.abs(endFile - startFile);
    const deltaRank = Math.abs(endRank - startRank);

    if (deltaRank === deltaFile) {
        return true;
    }

    return false;
}

function isForwardMove(params: MovePositions, color: Piece.PieceColor): boolean {
    const { fromRank, toRank } = params;
    const startRank = Tile.getTileRankIndex(fromRank);
    const endRank = Tile.getTileRankIndex(toRank);

    switch (color) {
        case Piece.PieceColor.White:
            return endRank > startRank;
        case Piece.PieceColor.Black:
            return endRank < startRank;
    }
}

interface PieceObstructionParams {
    startIndex: number;
    endTileKey: string;
    positionMap: PositionMap;
    indexIterator: (index: number) => number;
    tileKeyGenerator: (index: number) => string;
}

/**
 * Check if there is any piece between the start and end position.
 *
 * Will only check the tiles in between, iterating through them using the custom
 * function passed as parameter.
 */
function pieceObstruction(params: PieceObstructionParams): boolean {
    const { positionMap, startIndex, endTileKey, tileKeyGenerator, indexIterator } = params;
    const occupiedTiles = Tile.getOccupiedTiles(positionMap);

    let index: number = startIndex;
    let currentTileKey: string | undefined = undefined;

    while (currentTileKey !== endTileKey) {
        if (currentTileKey !== undefined) {
            if (occupiedTiles.includes(currentTileKey)) {
                return true;
            }
        }

        index = indexIterator(index);
        currentTileKey = tileKeyGenerator(index);
    }

    return false;
}

function sameRankMoveValidation(
    fromFile: Tile.TileFile,
    toFile: Tile.TileFile,
    fromRank: Tile.TileRank,
    positionMap: PositionMap,
    maxSteps?: number
) {
    const startFile = Tile.getTileFileIndex(fromFile);
    const endFile = Tile.getTileFileIndex(toFile);

    if (maxSteps !== undefined) {
        const delta = Math.abs(endFile - startFile);
        if (delta > maxSteps) {
            return false;
        }
    }

    const tileRank = fromRank;
    const endTileKey = Tile.createTileKey(tileRank, toFile);
    const indexIterator = (i: number) => (endFile > startFile ? i + 1 : i - 1);
    const tileKeyGenerator = (i: number) => Tile.createTileKey(tileRank, Tile.fileMap[i]);
    return !pieceObstruction({ endTileKey, startIndex: startFile, positionMap, indexIterator, tileKeyGenerator });
}

function sameFileMoveValidation(
    fromRank: Tile.TileRank,
    toRank: Tile.TileRank,
    fromFile: Tile.TileFile,
    positionMap: PositionMap,
    maxSteps?: number
) {
    const startRank = Tile.getTileRankIndex(fromRank);
    const endRank = Tile.getTileRankIndex(toRank);

    if (maxSteps !== undefined) {
        const delta = Math.abs(endRank - startRank);
        if (delta > maxSteps) {
            return false;
        }
    }

    const tileFile = fromFile;
    const endTileKey = Tile.createTileKey(toRank, tileFile);
    const indexIterator = (i: number) => (endRank > startRank ? i + 1 : i - 1);
    const tileKeyGenerator = (i: number) => Tile.createTileKey(Tile.rankMap[i], tileFile);
    return !pieceObstruction({ endTileKey, startIndex: startRank, positionMap, indexIterator, tileKeyGenerator });
}

function diagonalMoveValidation(
    fromFile: Tile.TileFile,
    toFile: Tile.TileFile,
    fromRank: Tile.TileRank,
    toRank: Tile.TileRank,
    positionMap: PositionMap,
    maxSteps?: number
) {
    const startFile = Tile.getTileFileIndex(fromFile);
    const endFile = Tile.getTileFileIndex(toFile);
    const startRank = Tile.getTileRankIndex(fromRank);
    const endRank = Tile.getTileRankIndex(toRank);

    if (maxSteps !== undefined) {
        const delta = Math.abs(endFile - startFile);
        if (delta !== Math.abs(endRank - startRank)) {
            // shouldn't happen if is diagonal
            return false;
        }

        if (delta > maxSteps) {
            return false;
        }
    }

    const deltaFile = normalize(endFile - startFile);
    const deltaRank = normalize(endRank - startRank);
    // check obstruction
    const endTileKey = Tile.createTileKey(toRank, toFile);
    const indexIterator = (i: number) => i + 1;
    const tileKeyGenerator = (i: number) => {
        const ri = startRank + i * deltaRank;
        const fi = startFile + i * deltaFile;
        return Tile.createTileKey(Tile.rankMap[ri], Tile.fileMap[fi]);
    };
    return !pieceObstruction({ endTileKey, startIndex: 0, positionMap, indexIterator, tileKeyGenerator });
}

// ==================================================================================================================================================================
// ==================================================================================================================================================================
// ===================================================   PIECE MOVEMENT VALIDATORS   ==================================================================================
// ==================================================================================================================================================================
// ==================================================================================================================================================================

export interface MovementValidationParams extends MovePositions {
    positionMap: PositionMap;
    piece: ChessPieceKey;
    attack: boolean;
    isCastle: boolean;
}

function rockMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile, positionMap } = params;

    if (fromRank === toRank) {
        return sameRankMoveValidation(fromFile, toFile, fromRank, positionMap);
    }

    if (fromFile === toFile) {
        return sameFileMoveValidation(fromRank, toRank, fromFile, positionMap);
    }

    return false;
}

function kingMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile, positionMap } = params;

    if (fromRank === toRank) {
        const maxSteps = params.isCastle ? KING_CASTLE_MAX_STEPS : KING_MAX_STEPS;
        return sameRankMoveValidation(fromFile, toFile, fromRank, positionMap, maxSteps);
    }

    if (fromFile === toFile) {
        return sameFileMoveValidation(fromRank, toRank, fromFile, positionMap, KING_MAX_STEPS);
    }

    if (isDiagonalMove(params)) {
        return diagonalMoveValidation(fromFile, toFile, fromRank, toRank, positionMap, KING_MAX_STEPS);
    }

    return false;
}

function queenMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile, positionMap } = params;

    if (fromRank === toRank) {
        return sameRankMoveValidation(fromFile, toFile, fromRank, positionMap);
    }

    if (fromFile === toFile) {
        return sameFileMoveValidation(fromRank, toRank, fromFile, positionMap);
    }

    if (isDiagonalMove(params)) {
        return diagonalMoveValidation(fromFile, toFile, fromRank, toRank, positionMap);
    }

    return false;
}

function bishopMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile, positionMap } = params;

    if (isDiagonalMove(params)) {
        return diagonalMoveValidation(fromFile, toFile, fromRank, toRank, positionMap);
    }

    return false;
}

function pawnMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile, positionMap, piece, attack } = params;

    const startingTile = Tile.createTileKey(fromRank, fromFile);
    const maxSteps = Piece.isInitialPosition(piece, startingTile) ? PAWN_MAX_STEPS_INITIAL : PAWN_MAX_STEPS;
    const color = Piece.getPieceColor(piece);

    if (isForwardMove(params, color)) {
        // attack diagonally
        if (attack) {
            const validAttack =
                isDiagonalMove(params) &&
                diagonalMoveValidation(fromFile, toFile, fromRank, toRank, positionMap, PAWN_MAX_STEPS);
            return validAttack;
        }

        if (fromFile === toFile) {
            return sameFileMoveValidation(fromRank, toRank, fromFile, positionMap, maxSteps);
        }
    }

    return false;
}

function knightMovementValidator(params: MovementValidationParams): boolean {
    const { fromRank, fromFile, toRank, toFile } = params;

    const startFile = Tile.getTileFileIndex(fromFile);
    const endFile = Tile.getTileFileIndex(toFile);
    const startRank = Tile.getTileRankIndex(fromRank);
    const endRank = Tile.getTileRankIndex(toRank);

    const deltaFile = Math.abs(endFile - startFile);
    const deltaRank = Math.abs(endRank - startRank);

    const vertical = deltaRank === 2 && deltaFile === 1;
    const horizontal = deltaRank === 1 && deltaFile === 2;

    return vertical || horizontal;
}

export function validateMove(moveParams: MovementValidationParams): boolean {
    const type = Piece.getPieceType(moveParams.piece);
    switch (type) {
        case Piece.PieceType.King:
            return kingMovementValidator(moveParams);
        case Piece.PieceType.Queen:
            return queenMovementValidator(moveParams);
        case Piece.PieceType.Rock:
            return rockMovementValidator(moveParams);
        case Piece.PieceType.Bishop:
            return bishopMovementValidator(moveParams);
        case Piece.PieceType.Pawn:
            return pawnMovementValidator(moveParams);
        case Piece.PieceType.Knight:
            return knightMovementValidator(moveParams);
    }
}

export function isCheck(positionMap: PositionMap, kingColor: Piece.PieceColor, isSimulation?: boolean): boolean {
    const occupiedTiles = Tile.getOccupiedTiles(positionMap);
    const king: ChessPieceKey = Piece.getKingOfColor(kingColor);

    const kingTile = Tile.getTileOfPiece(positionMap, king);
    if (kingTile === undefined) {
        if (!!isSimulation) {
            return true;
        }
        throw new Error(`No ${king} in game!`);
    }

    const kingRank = Tile.getRankFromKey(kingTile);
    const kingFile = Tile.getFileFromKey(kingTile);

    for (let i = 0; i < occupiedTiles.length; i++) {
        const tileKey = occupiedTiles[i];
        const piece = positionMap[tileKey];
        const color = Piece.getPieceColor(piece);
        if (color !== kingColor) {
            const pieceRank = Tile.getRankFromKey(tileKey);
            const pieceFile = Tile.getFileFromKey(tileKey);
            const moveParams: MovementValidationParams = {
                fromFile: pieceFile,
                fromRank: pieceRank,
                toFile: kingFile,
                toRank: kingRank,
                attack: true,
                piece,
                positionMap,
                isCastle: false,
            };

            if (validateMove(moveParams)) {
                return true;
            }
        }
    }

    return false;
}
