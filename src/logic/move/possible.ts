import * as Validation from "./validation";
import * as Tile from "../tile";
import * as Piece from "../piece";
import { PositionMap, ChessPieceKey, possibleMoveDirectionsMap } from "../../mappings";
import { CastleRequirements, MoveDirections, movePieceCopy } from "./move";

// ==================================================================================================================================================================
// ==================================================================================================================================================================
// ===================================================   PIECE MOVEMENT ITERATORS   =================================================================================
// ==================================================================================================================================================================
// ==================================================================================================================================================================

function iterateOverStraightMove<T>(
    index: number,
    direction: number,
    totalSteps: number,
    tileMmap: Tile.GenericPostioningMap<T>,
    callback: (newPosition: T) => boolean
) {
    let step = 0;
    while (step < totalSteps) {
        step++;
        const nextIndex = index + step * direction;
        const newPosition = tileMmap[nextIndex];
        if (newPosition === undefined) {
            // not tile in front
            break;
        }
        const canContinue = callback(newPosition);
        if (!canContinue) {
            break;
        }
    }
}

function iterateOverDiagonalMove(
    rankIndex: number,
    fileIndex: number,
    rankDirection: number,
    fileDirection: number,
    totalSteps: number,
    callback: (newRank: Tile.TileRank, leftFile: Tile.TileFile) => boolean
) {
    let step = 0;
    while (step < totalSteps) {
        step++;
        // rank
        const newRankIndex = rankIndex + step * rankDirection;
        const newRank = Tile.rankMap[newRankIndex];
        if (newRank === undefined) {
            // not tile forward
            break;
        }
        // file
        const leftFileIndex = fileIndex + step * rankDirection * fileDirection;
        const leftFile = Tile.fileMap[leftFileIndex];
        if (leftFile === undefined) {
            // no tile left
            break;
        }
        const canContinue = callback(newRank, leftFile);
        if (!canContinue) {
            break;
        }
    }
}

function iterateStraightRankMoves(
    rankIndex: number,
    direction: number,
    maxSteps: number | undefined,
    callback: (newRank: Tile.TileRank) => boolean
) {
    const totalSteps = maxSteps !== undefined ? maxSteps : Validation.MAX_STEPS;
    iterateOverStraightMove(rankIndex, direction, totalSteps, Tile.rankMap, callback);
}

function iterateStraightFileMoves(
    fileIndex: number,
    direction: number,
    maxSteps: number | undefined,
    callback: (newFile: Tile.TileFile) => boolean
) {
    const totalSteps = maxSteps !== undefined ? maxSteps : Validation.MAX_STEPS;
    iterateOverStraightMove(fileIndex, direction, totalSteps, Tile.fileMap, callback);
}

function iterateLeftDiagonalMoves(
    rankIndex: number,
    fileIndex: number,
    direction: number,
    maxSteps: number | undefined,
    callback: (newRank: Tile.TileRank, leftFile: Tile.TileFile) => boolean
) {
    const totalSteps = maxSteps !== undefined ? maxSteps : Validation.MAX_STEPS;
    iterateOverDiagonalMove(rankIndex, fileIndex, direction, -1, totalSteps, callback);
}

function iterateRightDiagonalMoves(
    rankIndex: number,
    fileIndex: number,
    direction: number,
    maxSteps: number | undefined,
    callback: (newRank: Tile.TileRank, leftFile: Tile.TileFile) => boolean
) {
    const totalSteps = maxSteps !== undefined ? maxSteps : Validation.MAX_STEPS;
    iterateOverDiagonalMove(rankIndex, fileIndex, direction, 1, totalSteps, callback);
}

// ==================================================================================================================================================================
// ==================================================================================================================================================================
// ===================================================   PIECE MOVEMENT POSSIBILITIES   =============================================================================
// ==================================================================================================================================================================
// ==================================================================================================================================================================

interface PossibleMovesParams {
    color: Piece.PieceColor;
    file: Tile.TileFile;
    rank: Tile.TileRank;
    forward: number;
    backward: number;
    rankIndex: number;
    fileIndex: number;
}

function getPossibleMovesParams(tileKey: string, piece: ChessPieceKey): PossibleMovesParams {
    const color = Piece.getPieceColor(piece);
    const file = Tile.getFileFromKey(tileKey);
    const rank = Tile.getRankFromKey(tileKey);
    const forward = Piece.getForwardDirection(color);
    return {
        color,
        file,
        rank,
        forward,
        backward: forward * -1,
        rankIndex: Tile.getTileRankIndex(rank),
        fileIndex: Tile.getTileFileIndex(file),
    };
}

/**
 * Checks if the move would pose a threat on the king
 *
 * If the move opens a check on the king, then return `true`
 */
function hypoteticalCheck(positionMap: PositionMap, from: string, to: string, piece: ChessPieceKey): boolean {
    const color = Piece.getPieceColor(piece);
    const nextPositionMap = movePieceCopy(positionMap, from, to, piece);
    return Validation.isCheck(nextPositionMap, color, true);
}

type MoveDirectionPiece = Piece.RockPiece | Piece.BishopPiece | Piece.QueenPiece;
/**
 * Get the possible moves from the direction groupings
 */
function getPossibleMovesFromDirections(
    piece: MoveDirectionPiece,
    tileKey: string,
    positionMap: PositionMap
): string[] {
    const possibleMoves: string[] = [];
    const directionMoves = possibleMoveDirectionsMap[piece][tileKey];
    const pmParams = getPossibleMovesParams(tileKey, piece);

    const directions = Object.keys(directionMoves);
    for (let di = 0; di < directions.length; di++) {
        const direction = directions[di] as keyof MoveDirections;
        const moves = directionMoves[direction];
        if (moves === undefined) {
            // should not happen
            continue;
        }
        for (let mi = 0; mi < moves.length; mi++) {
            const possibleTileKey = moves[mi];
            const targetPiece = positionMap[possibleTileKey];
            const selfCheck = hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece);
            if (targetPiece !== undefined) {
                // piece in front
                if (Piece.getPieceColor(targetPiece) !== pmParams.color && !selfCheck) {
                    possibleMoves.push(possibleTileKey);
                }
                break;
            }
            if (selfCheck) {
                // illegal move
                continue;
            }
            possibleMoves.push(possibleTileKey);
        }
    }
    return possibleMoves;
}

function getPossibleMovesForPawn(piece: Piece.PawnPiece, tileKey: string, positionMap: PositionMap): string[] {
    const possibleMoves: string[] = [];
    const directionMoves = possibleMoveDirectionsMap[piece][tileKey];
    const pmParams = getPossibleMovesParams(tileKey, piece);

    const addAttack = (moves: string[] | undefined) => {
        if (moves === undefined) {
            return;
        }
        for (let mi = 0; mi < moves.length; mi++) {
            const possibleTileKey = moves[mi];
            const targetPiece = positionMap[possibleTileKey];
            const selfCheck = hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece);
            if (targetPiece !== undefined && Piece.getPieceColor(targetPiece) !== pmParams.color && !selfCheck) {
                // has a piece to attack and is not check
                possibleMoves.push(possibleTileKey);
            }
        }
    };

    const addMove = (moves: string[] | undefined) => {
        if (moves === undefined) {
            return;
        }
        for (let mi = 0; mi < moves.length; mi++) {
            const possibleTileKey = moves[mi];
            const targetPiece = positionMap[possibleTileKey];
            const selfCheck = hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece);
            if (targetPiece !== undefined) {
                // break if there's a piece on front
                break;
            }
            if (!selfCheck) {
                possibleMoves.push(possibleTileKey);
            }
        }
    };

    const directions = Object.keys(directionMoves);
    for (let di = 0; di < directions.length; di++) {
        const direction = directions[di] as keyof MoveDirections;
        switch (direction) {
            case "forward":
            case "backward":
                addMove(directionMoves[direction]);
                break;
            case "backwardLeft":
            case "backwardRight":
            case "forwardLeft":
            case "forwardRight":
                addAttack(directionMoves[direction]);
                break;
        }
    }

    return possibleMoves;
}

function getPossibleMovesForQueen(piece: Piece.QueenPiece, tileKey: string, positionMap: PositionMap): string[] {
    return getPossibleMovesFromDirections(piece, tileKey, positionMap);
}

function getPossibleMovesForRock(piece: Piece.RockPiece, tileKey: string, positionMap: PositionMap): string[] {
    return getPossibleMovesFromDirections(piece, tileKey, positionMap);
}

function getPossibleMovesForBishop(piece: Piece.BishopPiece, tileKey: string, positionMap: PositionMap): string[] {
    return getPossibleMovesFromDirections(piece, tileKey, positionMap);
}

function getPossibleMovesForKing(
    piece: Piece.KingPiece,
    tileKey: string,
    positionMap: PositionMap,
    castleRequirements: CastleRequirements
): string[] {
    const possibleMoves: string[] = [];
    const directionMoves = possibleMoveDirectionsMap[piece][tileKey];
    const pmParams = getPossibleMovesParams(tileKey, piece);
    const isCheck = Validation.isCheck(positionMap, pmParams.color, true);

    const addMove = (moves: string[] | undefined) => {
        if (moves === undefined) {
            return;
        }
        for (let mi = 0; mi < moves.length; mi++) {
            const possibleTileKey = moves[mi];
            const targetPiece = positionMap[possibleTileKey];
            const selfCheck = hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece);
            if (targetPiece !== undefined) {
                // piece in front
                if (Piece.getPieceColor(targetPiece) !== pmParams.color && !selfCheck) {
                    possibleMoves.push(possibleTileKey);
                }
                break;
            }
            if (selfCheck) {
                // illegal move
                continue;
            }
            possibleMoves.push(possibleTileKey);
        }
    };

    const addLateralMove = (moves: string[] | undefined, direction: string) => {
        if (moves === undefined) {
            return;
        }

        const relevantRockMoves =
            (pmParams.color === Piece.PieceColor.White && direction === "right") ||
            (pmParams.color === Piece.PieceColor.Black && direction === "left")
                ? castleRequirements.rightRockMoved
                : castleRequirements.leftRockMoved;

        const canCastle = !castleRequirements.kingHasMoved && !isCheck && !relevantRockMoves;
        const maxFileSteps = canCastle ? Validation.KING_CASTLE_MAX_STEPS : Validation.KING_MAX_STEPS;
        const limit = Math.min(moves.length, maxFileSteps);

        for (let mi = 0; mi < limit; mi++) {
            const possibleTileKey = moves[mi];
            const targetPiece = positionMap[possibleTileKey];
            // for king, lateral moves break on first check (castling rules)
            if (hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece)) {
                // illegal move
                break;
            }
            if (targetPiece !== undefined) {
                // piece in front
                if (Piece.getPieceColor(targetPiece) !== pmParams.color) {
                    possibleMoves.push(possibleTileKey);
                }
                break;
            }
            possibleMoves.push(possibleTileKey);
        }
    };

    const directions = Object.keys(directionMoves);
    for (let di = 0; di < directions.length; di++) {
        const direction = directions[di] as keyof MoveDirections;
        switch (direction) {
            case "forward":
            case "backward":
            case "backwardLeft":
            case "forwardLeft":
            case "backwardRight":
            case "forwardRight":
                addMove(directionMoves[direction]);
                break;
            case "left":
            case "right":
                addLateralMove(directionMoves[direction], direction);
                break;
        }
    }

    return possibleMoves;
}

function getPossibleMovesForKnight(piece: Piece.KnightPiece, tileKey: string, positionMap: PositionMap): string[] {
    const possibleMoves: string[] = [];

    const pmParams = getPossibleMovesParams(tileKey, piece);
    const moves = possibleMoveDirectionsMap[piece][tileKey];
    for (let i = 0; i < moves.length; i++) {
        const possibleTileKey = moves[i];
        const targetPiece = positionMap[possibleTileKey];
        if (targetPiece === undefined || Piece.getPieceColor(targetPiece) !== pmParams.color) {
            if (!hypoteticalCheck(positionMap, tileKey, possibleTileKey, piece)) {
                possibleMoves.push(possibleTileKey);
            }
        }
    }

    return possibleMoves;
}

export function getCurrentPossibleMoves(
    tileKey: string,
    positionMap: PositionMap,
    castleRequirements: CastleRequirements
): string[] {
    const piece = positionMap[tileKey];
    const type = Piece.getPieceType(piece);
    switch (type) {
        case Piece.PieceType.King:
            return getPossibleMovesForKing(piece as Piece.KingPiece, tileKey, positionMap, castleRequirements);
        case Piece.PieceType.Bishop:
            return getPossibleMovesForBishop(piece as Piece.BishopPiece, tileKey, positionMap);
        case Piece.PieceType.Pawn:
            return getPossibleMovesForPawn(piece as Piece.PawnPiece, tileKey, positionMap);
        case Piece.PieceType.Queen:
            return getPossibleMovesForQueen(piece as Piece.QueenPiece, tileKey, positionMap);
        case Piece.PieceType.Rock:
            return getPossibleMovesForRock(piece as Piece.RockPiece, tileKey, positionMap);
        case Piece.PieceType.Knight:
            return getPossibleMovesForKnight(piece as Piece.KnightPiece, tileKey, positionMap);
    }
}
