import { PieceMap, chessPiecesDefaultMap, chessPiecesGifMap } from "../mappings";

export class ThemeManager {
    private m_currentThemeIndex: number;
    private m_themes: PieceMap[];

    constructor() {
        this.m_currentThemeIndex = 0;
        this.m_themes = [chessPiecesDefaultMap, chessPiecesGifMap];
    }

    get theme(): PieceMap {
        return this.m_themes[this.m_currentThemeIndex];
    }

    toggleTheme() {
        const currentThemeIndex = this.m_currentThemeIndex;
        const length = this.m_themes.length;
        this.m_currentThemeIndex = (currentThemeIndex + 1) % length;
    }
}
