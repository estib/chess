import { ChessPieceKey, PositionMap } from "../mappings";
import { PieceColor, getPieceColor } from "./piece";

export const RANK_DIMENSION = 8;
export const FILE_DIMENSION = 8;

export enum TileColor {
    White,
    Black,
}

export enum TileRank {
    _1 = "1",
    _2 = "2",
    _3 = "3",
    _4 = "4",
    _5 = "5",
    _6 = "6",
    _7 = "7",
    _8 = "8",
}

export enum TileFile {
    A = "A",
    B = "B",
    C = "C",
    D = "D",
    E = "E",
    F = "F",
    G = "G",
    H = "H",
}

export type GenericPostioningMap<T> = {
    [key: number]: T;
};

export type RankMap = GenericPostioningMap<TileRank>;

export type FileMap = GenericPostioningMap<TileFile>;

export const rankMap: RankMap = {
    0: TileRank._1,
    1: TileRank._2,
    2: TileRank._3,
    3: TileRank._4,
    4: TileRank._5,
    5: TileRank._6,
    6: TileRank._7,
    7: TileRank._8,
};

export const fileMap: FileMap = {
    0: TileFile.A,
    1: TileFile.B,
    2: TileFile.C,
    3: TileFile.D,
    4: TileFile.E,
    5: TileFile.F,
    6: TileFile.G,
    7: TileFile.H,
};

export interface TileInformation {
    rank: TileRank;
    file: TileFile;
    piece: ChessPieceKey | undefined;
}

export function generateTileInformation(piece: ChessPieceKey | undefined, tileKey: string): TileInformation {
    return {
        piece,
        file: getFileFromKey(tileKey),
        rank: getRankFromKey(tileKey),
    };
}

export function getTileColorClass(color: TileColor, highlighted: boolean): string {
    switch (color) {
        case TileColor.Black:
            return !highlighted ? "game-board-tile-black" : "game-board-tile-black-highlighted";
        case TileColor.White:
            return !highlighted ? "game-board-tile-white" : "game-board-tile-white-highlighted";
        default:
            return "game-board-tile";
    }
}

export function getTileRankIndex(rank: TileRank): number {
    switch (rank) {
        case TileRank._1:
            return 0;
        case TileRank._2:
            return 1;
        case TileRank._3:
            return 2;
        case TileRank._4:
            return 3;
        case TileRank._5:
            return 4;
        case TileRank._6:
            return 5;
        case TileRank._7:
            return 6;
        case TileRank._8:
            return 7;
    }
}

export function getTileFileIndex(file: TileFile): number {
    switch (file) {
        case TileFile.A:
            return 0;
        case TileFile.B:
            return 1;
        case TileFile.C:
            return 2;
        case TileFile.D:
            return 3;
        case TileFile.E:
            return 4;
        case TileFile.F:
            return 5;
        case TileFile.G:
            return 6;
        case TileFile.H:
            return 7;
    }
}

export function getTileColor(rankIndex: number, fileIndex: number): TileColor {
    if (rankIndex % 2 === 0) {
        return fileIndex % 2 !== 0 ? TileColor.White : TileColor.Black;
    }
    return fileIndex % 2 === 0 ? TileColor.White : TileColor.Black;
}

export function getRankFromKey(tileKey: string): TileRank {
    if (tileKey.length !== 2) {
        throw new Error(`Invalid tile key: ${tileKey}`);
    }

    const rank = tileKey[1] as TileRank;

    switch (rank) {
        case TileRank._1:
            return TileRank._1;
        case TileRank._2:
            return TileRank._2;
        case TileRank._3:
            return TileRank._3;
        case TileRank._4:
            return TileRank._4;
        case TileRank._5:
            return TileRank._5;
        case TileRank._6:
            return TileRank._6;
        case TileRank._7:
            return TileRank._7;
        case TileRank._8:
            return TileRank._8;
        default:
            throw new Error(`Invalid tile key: ${tileKey}`);
    }
}

export function getFileFromKey(tileKey: string): TileFile {
    if (tileKey.length !== 2) {
        throw new Error(`Invalid tile key: ${tileKey}`);
    }

    const file = tileKey[0] as TileFile;

    switch (file) {
        case TileFile.A:
            return TileFile.A;
        case TileFile.B:
            return TileFile.B;
        case TileFile.C:
            return TileFile.C;
        case TileFile.D:
            return TileFile.D;
        case TileFile.E:
            return TileFile.E;
        case TileFile.F:
            return TileFile.F;
        case TileFile.G:
            return TileFile.G;
        case TileFile.H:
            return TileFile.H;
        default:
            throw new Error(`Invalid tile key: ${tileKey}`);
    }
}

export function getOccupiedTiles(positionMap: PositionMap): string[] {
    return Object.keys(positionMap);
}

export function getOccupiedTilesOfColor(positionMap: PositionMap, color: PieceColor): string[] {
    const occupiedTiles = getOccupiedTiles(positionMap);
    return occupiedTiles.filter((tileKey) => {
        const piece = positionMap[tileKey];
        const pieceColor = getPieceColor(piece);
        return pieceColor === color;
    });
}

export function getTileOfPiece(positionMap: PositionMap, piece: ChessPieceKey): string | undefined {
    const occupiedTiles = getOccupiedTiles(positionMap);
    return occupiedTiles.find((t) => positionMap[t] === piece);
}

export function createTileKey(rank: TileRank, file: TileFile): string {
    return `${file}${rank}`;
}

function hashPiece(piece: ChessPieceKey): string {
    switch (piece) {
        case "king-black":
            return "a";
        case "king-white":
            return "b";
        case "knight-black":
            return "c";
        case "knight-white":
            return "d";
        case "pawn-black":
            return "e";
        case "pawn-white":
            return "f";
        case "queen-black":
            return "g";
        case "queen-white":
            return "h";
        case "rock-black-left":
            return "i";
        case "rock-black-right":
            return "j";
        case "rock-white-left":
            return "k";
        case "rock-white-right":
            return "l";
        case "bishop-black":
            return "m";
        case "bishop-white":
            return "n";
    }
}
/**
 * Hash position map
 */
export function hashPositionMap(positionMap: PositionMap): string {
    const occupiedTiles = getOccupiedTiles(positionMap);
    occupiedTiles.sort();
    const buffer: string[] = [];
    for (let i = 0; i < occupiedTiles.length; i++) {
        const t = occupiedTiles[i];
        const piece = positionMap[t];
        if (piece === undefined) {
            // should not happen
            console.error(`Not occupied tile in hash: ${t}`);
            continue;
        }
        const h = hashPiece(piece);
        buffer.push(t);
        buffer.push(h);
    }
    return buffer.join("");
}

interface TilePositionIndex {
    fileIndex: number;
    rankIndex: number;
}

export function getTilePositionIndex(tileKey: string): TilePositionIndex {
    const file = getFileFromKey(tileKey);
    const rank = getRankFromKey(tileKey);
    const fileIndex = getTileFileIndex(file);
    const rankIndex = getTileRankIndex(rank);
    return { fileIndex, rankIndex };
}

/**
 * Get absolute distance between 2 tile keys
 */
export function distanceBetween(tKA: string, tKB: string): number {
    const positionA = getTilePositionIndex(tKA);
    const positionB = getTilePositionIndex(tKB);
    return Math.sqrt(
        Math.pow(positionB.fileIndex - positionA.fileIndex, 2) + Math.pow(positionB.rankIndex - positionA.rankIndex, 2)
    );
}
