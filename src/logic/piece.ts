import { pieceColors, ChessPieceKey, pieceTypes, initialPositionMap, pieceValue } from "../mappings";
import { getRankFromKey, TileRank } from "./tile";

export enum PieceColor {
    Black = "black",
    White = "white",
}

export enum PieceType {
    King,
    Queen,
    Bishop,
    Knight,
    Rock,
    Pawn,
}

export type RockPiece = "rock-white-left" | "rock-white-right" | "rock-black-left" | "rock-black-right";
export type BishopPiece = "bishop-black" | "bishop-white";
export type QueenPiece = "queen-black" | "queen-white";
export type KnightPiece = "knight-black" | "knight-white";
export type PawnPiece = "pawn-black" | "pawn-white";
export type KingPiece = "king-black" | "king-white";

export function getPieceColor(piece: ChessPieceKey): PieceColor {
    const color = pieceColors[piece];
    switch (color) {
        case "black":
            return PieceColor.Black;
        case "white":
            return PieceColor.White;
        default:
            throw new Error(`Invalid color mapped for: ${piece}`);
    }
}

export function getContraryColor(color: PieceColor): PieceColor {
    switch (color) {
        case PieceColor.White:
            return PieceColor.Black;
        case PieceColor.Black:
            return PieceColor.White;
    }
}

export function isPromotionRank(tileKey: string, color: PieceColor): boolean {
    switch (color) {
        case PieceColor.Black:
            return getRankFromKey(tileKey) === TileRank._1;
        case PieceColor.White:
            return getRankFromKey(tileKey) === TileRank._8;
    }
}

export function getPieceType(piece: ChessPieceKey): PieceType {
    const type = pieceTypes[piece];
    switch (type) {
        case "king":
            return PieceType.King;
        case "queen":
            return PieceType.Queen;
        case "bishop":
            return PieceType.Bishop;
        case "knight":
            return PieceType.Knight;
        case "rock":
            return PieceType.Rock;
        case "pawn":
            return PieceType.Pawn;
        default:
            throw new Error(`Invalid piece type for piece: ${piece}`);
    }
}

export function getPieceValue(piece: ChessPieceKey): number {
    return pieceValue[piece];
}

export function sameColorPieces(pieceA: ChessPieceKey | undefined, pieceB: ChessPieceKey | undefined): boolean {
    if (pieceA !== undefined && pieceB !== undefined) {
        const colorA = getPieceColor(pieceA);
        const colorB = getPieceColor(pieceB);
        return colorA === colorB;
    }
    return false;
}

export function isInitialPosition(piece: ChessPieceKey, tileKey: string): boolean {
    return initialPositionMap[tileKey] === piece;
}

export function getKingOfColor(color: PieceColor): ChessPieceKey {
    switch (color) {
        case PieceColor.Black:
            return "king-black";
        case PieceColor.White:
            return "king-white";
    }
}

export function getQueenOfColor(color: PieceColor): ChessPieceKey {
    switch (color) {
        case PieceColor.Black:
            return "queen-black";
        case PieceColor.White:
            return "queen-white";
    }
}

export function getForwardDirection(color: PieceColor): number {
    switch (color) {
        case PieceColor.White:
            return 1;
        case PieceColor.Black:
            return -1;
    }
}

export function isLeftRock(piece: ChessPieceKey): boolean {
    return piece === "rock-black-left" || piece === "rock-white-left";
}

export function isRightRock(piece: ChessPieceKey): boolean {
    return piece === "rock-black-right" || piece === "rock-white-right";
}

export function isKing(piece: ChessPieceKey): piece is KingPiece {
    return piece === "king-black" || piece === "king-white";
}

export function isPawn(piece: ChessPieceKey): piece is PawnPiece {
    return piece === "pawn-black" || piece === "pawn-white";
}

export function getCastlingRock(king: ChessPieceKey, direction: number): RockPiece {
    const color = getPieceColor(king);
    switch (color) {
        case PieceColor.Black:
            return direction < 0 ? "rock-black-left" : "rock-black-right";
        case PieceColor.White:
            return direction > 0 ? "rock-white-left" : "rock-white-right";
    }
}

export function getCastlingTile(king: ChessPieceKey, direction: number): string {
    const color = getPieceColor(king);
    switch (color) {
        case PieceColor.Black:
            return direction < 0 ? "F8" : "D8";
        case PieceColor.White:
            return direction > 0 ? "D1" : "F1";
    }
}
