import React from "react";

export interface StatusProps {
    text: string;
}

export const Status: React.FC<StatusProps> = (props) => {
    return <div className="status-container">{props.text}</div>;
};
