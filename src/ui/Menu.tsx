import React from "react";

const MENU_TITLE = "DUMB CHESS";
const NEW_GAME_BUTTON_LABEL = "START NEW GAME";

// ########################################################################################################################
// MENU BUTTON

interface MenuButtonProps {
    label: string;
    onClick: () => void;
}

const MenuButton: React.FC<MenuButtonProps> = (props) => {
    return (
        <div className="menu-button" onClick={props.onClick}>
            {props.label}
        </div>
    );
};

// ########################################################################################################################
// MENU
export interface MenuProps {
    show: boolean;
    onNewGame: () => void;
    canClose: boolean;
    onCLose: () => void;
}

export class Menu extends React.Component<MenuProps, {}> {
    render(): JSX.Element {
        const cssClass = this.props.show ? "menu-container" : "menu-container-hidden";
        return (
            <div className={cssClass}>
                {this.props.canClose ? (
                    <div className="menu-close" onClick={() => this.props.onCLose()}>
                        close
                    </div>
                ) : (
                    <React.Fragment />
                )}
                <div className="menu-content">
                    <div className="menu-row">
                        <div className="menu-title">{MENU_TITLE}</div>
                    </div>
                    <div className="menu-row">
                        <MenuButton label={NEW_GAME_BUTTON_LABEL} onClick={() => this.props.onNewGame()} />
                    </div>
                </div>
            </div>
        );
    }
}
