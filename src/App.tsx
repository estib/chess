import React from "react";
import * as Logic from "./logic";
import { initialPositionMap, PositionMap, ChessPieceKey, PieceMap } from "./mappings";
import { GameBoard } from "./gameElement/GameBoard";
import { Menu, Status } from "./ui";

const TIME_INTERVAL_AI_MOVE = 0.5;
enum GameMode {
    Start,
    SelfGame,
    AIGame,
    Debug,
}

interface GameFieldState {
    positionMap: PositionMap;
    lastTileSelected: Logic.TileInformation | undefined;
    invert: boolean;
    showMenu: boolean;
    theme: PieceMap;
    status: string;
    highlightedTiles: string[];
}

class GameField extends React.Component<{}, GameFieldState> {
    private manager: Logic.GameManager;
    private themeManager: Logic.ThemeManager;
    private AIColor: Logic.PieceColor | undefined;
    private mode: GameMode;

    constructor(props: {}) {
        super(props);
        this.manager = new Logic.GameManager();
        this.themeManager = new Logic.ThemeManager();
        this.AIColor = undefined;
        this.mode = GameMode.Start;
        const positionMap: PositionMap = { ...initialPositionMap };

        this.state = {
            positionMap,
            lastTileSelected: undefined,
            invert: false,
            showMenu: true,
            theme: this.themeManager.theme,
            status: this.manager.getStatusLabel(),
            highlightedTiles: [],
        };
    }

    private getPositionMap(): PositionMap {
        return { ...this.state.positionMap };
    }

    startNewGame() {
        this.mode = GameMode.AIGame;
        this.AIColor = Logic.PieceColor.Black;
        this.manager.startNewGame();

        this.setState({
            positionMap: initialPositionMap,
            lastTileSelected: undefined,
            invert: false,
            showMenu: false,
            theme: this.themeManager.theme,
            status: this.manager.getStatusLabel(),
            highlightedTiles: [],
        });
    }

    startSelfGame() {
        this.mode = GameMode.SelfGame;
        this.manager.startNewGame();
        this.setState({ showMenu: false }, () => this.selfPlay());
    }

    undo() {
        const positionMap = this.getPositionMap();
        this.manager.undoMove(positionMap);
        const status = this.manager.getStatusLabel();
        this.setState({ positionMap, status });
    }

    toggleInvertBoard() {
        const invert = this.state.invert;
        this.setState({ invert: !invert });
    }

    toggleTheme() {
        this.themeManager.toggleTheme();
        this.setState({ theme: this.themeManager.theme });
    }

    menu() {
        if (this.mode !== GameMode.Start) {
            const menu = this.state.showMenu;
            this.setState({ showMenu: !menu });
        }
    }

    toggleSelfGame() {
        if (this.mode !== GameMode.SelfGame) {
            this.mode = GameMode.SelfGame;
            this.AIColor = undefined;
            this.selfPlay();
        } else {
            this.stopSelfGame();
        }
    }

    debugState() {
        this.AIColor = undefined;
        this.mode = GameMode.Debug;
        this.setState({ showMenu: false });
    }

    stopSelfGame() {
        this.mode = GameMode.Debug;
        console.log("stop");
    }

    onCheckMate() {
        const moves = this.manager.getMovesString();
        console.log(moves);
        this.stopSelfGame();
        this.setState({ status: this.manager.getStatusLabel(), showMenu: true });
    }

    onStale() {
        const moves = this.manager.getMovesString();
        console.log(moves);
        this.stopSelfGame();
        this.manager.staleMate();
        this.setState({ status: this.manager.getStatusLabel(), showMenu: true });
    }

    AIMove() {
        const positionMap = this.getPositionMap();

        const move = (from: string, to: string) => this.autoMove(from, to);
        const mate = () => this.onCheckMate();
        const stale = () => this.onStale();

        this.manager.AIMove(positionMap, move, mate, stale);
    }

    async selfPlay() {
        if (this.mode !== GameMode.SelfGame) {
            return;
        }

        this.AIMove();

        // repeat
        if (this.mode === GameMode.SelfGame) {
            await this.manager.sleep(TIME_INTERVAL_AI_MOVE);
            this.selfPlay();
        }
    }

    keyUpHandler(e: KeyboardEvent) {
        switch (e.keyCode) {
            case 80: // p
                const moves = this.manager.getMovesString();
                console.log(moves);
                console.log(JSON.stringify(this.state.positionMap));
                break;
            case 65: // a
                this.AIMove();
                break;
            case 83: // s
                this.toggleSelfGame();
                break;
            case 68: // d
                this.debugState();
                break;
            case 73: // i
                this.toggleInvertBoard();
                break;
            case 84: // t
                this.toggleTheme();
                break;
            case 32: // space
                this.menu();
                break;
            case 37: // arrow-left
                this.undo();
                break;
            default:
                console.warn(e.keyCode);
                break;
        }
    }

    componentDidMount() {
        document.addEventListener("keyup", (e) => this.keyUpHandler(e));
    }

    componentWillUnmount() {
        document.removeEventListener("keyup", (e) => this.keyUpHandler(e));
    }

    getPieceForTile(rank: Logic.TileRank, file: Logic.TileFile): ChessPieceKey | undefined {
        const key = Logic.createTileKey(rank, file);
        return this.state.positionMap[key];
    }

    tryMove(
        piece: ChessPieceKey,
        lastTile: Logic.TileInformation,
        currentTile: Logic.TileInformation
    ): PositionMap | undefined {
        const positionMap = this.getPositionMap();
        const { canMove, color, castleRequirements, isCheck } = this.manager.getTurnAnalysis(piece);
        if (canMove) {
            const params: Logic.MoveDeterminationParams = {
                previousTile: lastTile,
                currentTile,
                positionMap,
                castleRequirements,
                isInCheck: isCheck,
                isCastle: Logic.isCastleMove(lastTile, currentTile),
                color,
            };
            const validMove = Logic.canMovePieceToTile(params);
            if (validMove) {
                this.manager.performMove(positionMap, lastTile, currentTile, piece);
                return positionMap;
            }
        }
        return undefined;
    }

    autoMove(from: string, to: string) {
        const positionMap = this.getPositionMap();
        const piece = positionMap[from];
        const targetPiece = positionMap[to];
        const lastTile = Logic.generateTileInformation(piece, from);
        const currentTile = Logic.generateTileInformation(targetPiece, to);

        const newPositionMap = this.tryMove(piece, lastTile, currentTile) || positionMap;
        const status = this.manager.getStatusLabel();

        this.setState({
            lastTileSelected: currentTile,
            positionMap: newPositionMap,
            status,
            highlightedTiles: [from, to],
        });
    }

    afterClick() {
        if (this.mode === GameMode.AIGame && this.AIColor === this.manager.getColorTurn()) {
            this.manager.sleep(TIME_INTERVAL_AI_MOVE).then(() => this.AIMove());
        }
    }

    clickHandler(currentTile: Logic.TileInformation) {
        const lastTile = this.state.lastTileSelected;
        const { canMove, castleRequirements } = this.manager.getTurnAnalysis(currentTile.piece);

        let positionMap = this.getPositionMap();
        let highlightedTiles: string[] = [];

        if (canMove) {
            // selecting piece to move
            const currentKey = Logic.createTileKey(currentTile.rank, currentTile.file);
            highlightedTiles = Logic.getCurrentPossibleMoves(currentKey, positionMap, castleRequirements);
        }

        if (lastTile !== undefined && lastTile.piece !== undefined) {
            // selecting piece destination
            const newMap = this.tryMove(lastTile.piece, lastTile, currentTile);
            if (newMap !== undefined) {
                positionMap = newMap;
                highlightedTiles = [];
            }
        }

        this.setState(
            {
                lastTileSelected: currentTile,
                positionMap,
                status: this.manager.getStatusLabel(),
                highlightedTiles,
            },
            () => this.afterClick()
        );
    }

    render(): JSX.Element {
        const blockBoard =
            this.mode === GameMode.SelfGame ||
            (this.mode === GameMode.AIGame && this.AIColor === this.manager.getColorTurn()) ||
            this.state.showMenu;
        const isStart = this.mode === GameMode.Start;
        const isDebug = this.mode === GameMode.Debug;
        return (
            <div className="game-field">
                <Status text={this.state.status} />
                <GameBoard
                    hide={isStart}
                    clickHandler={(t) => this.clickHandler(t)}
                    getPieceForTile={(r, f) => this.getPieceForTile(r, f)}
                    invert={this.state.invert}
                    theme={this.state.theme}
                    block={blockBoard}
                    highlightedTiles={this.state.highlightedTiles}
                />
                {!isStart ? (
                    <div className="game-field-label" onClick={() => this.menu()}>
                        {isDebug ? "DEBUG" : "menu"}
                    </div>
                ) : (
                    <React.Fragment />
                )}
                <Menu
                    show={this.state.showMenu}
                    onNewGame={() => this.startNewGame()}
                    canClose={!isStart}
                    onCLose={() => this.menu()}
                />
            </div>
        );
    }
}

function App() {
    return (
        <div className="App">
            <GameField />
        </div>
    );
}

export default App;
