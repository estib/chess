import { GameAI } from "./src/logic/ai";
import { AIWorkerMessageParameters, AIWorkerMessageType } from "./src/logic/aiWorker";

const m_AI = new GameAI();

onmessage = function (e) {
    const request = e.data as AIWorkerMessageParameters;
    switch (request.type) {
        case AIWorkerMessageType.GetNextMove: {
            const { positionMap, colorTurn, castleRequirements, previousPositions } = request;
            const nextMove = m_AI.getNextMove(positionMap, colorTurn, castleRequirements, previousPositions);
            postMessage({ type: request.type, nextMove });
        }
    }
};
