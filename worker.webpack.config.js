const path = require("path");

const RESOURCES_DIR = "resources";

module.exports = {
    target: "web",
    mode: "none",
    devtool: "source-map",
    entry: [path.resolve(__dirname, "./worker.ts")],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "less-loader",
                        options: {
                            strictMath: true,
                            noIeCompat: true,
                        },
                    },
                ],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",
                    },
                ],
            },
            {
                test: /\.(ttf|eot|woff|woff2|otf)(\?v=[^\.]+\.[^\.]+\.[^\.]+)?$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash:8].[ext]",
                    },
                },
            },
            {
                test: /\.(jpe?g|svg|png)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[hash].[ext]",
                    },
                },
            },
            {
                test: /\.html$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                    },
                },
            },
        ],
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    output: {
        filename: "worker.js",
        path: path.resolve(__dirname, RESOURCES_DIR),
    },
};
